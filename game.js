var count;
var img_src;
window.addEventListener("load", init);
function init() {
    count = 1;
    img_src = "image.jpeg";
    addLevel();
    addSameImages();
    addDifferentImage();
}
function addSameImages() {
    for (var i = 0; i < count; i++) {
        var margin_left = generateRandom(screen.width / 2 - 200);
        var margin_top = generateRandom(screen.height - 300);
        for (var j = 0; j < 2; j++) {
            var img = document.createElement("img");
            img.setAttribute("src", img_src);
            img.style.marginTop = margin_top + "px";
            img.style.marginLeft = margin_left + "px";
            img.addEventListener("click", fail);
            if (j == 0) {
                var right = document.getElementById("right");
                right.appendChild(img);
            }
            else {
                var left = document.getElementById("left");
                left.appendChild(img);
            }
        }
    }
}
function addDifferentImage() {
    var margin_left = generateRandom(screen.width / 2 - 200);
    var margin_top = generateRandom(screen.height - 300);
    var img = document.createElement("img");
    img.setAttribute("src", img_src);
    img.style.marginTop = margin_top + "px";
    img.style.marginLeft = margin_left + "px";
    img.addEventListener("click", success);
    var left = document.getElementById("left");
    left.appendChild(img);
}
function addLevel() {
    var level = document.createElement("h3");
    level.innerHTML = "level: " + count;
    level.style.marginTop = "30px";
    level.style.marginLeft = "30px";
    level.style.color = "red";
    document.body.appendChild(level);
    var left = document.getElementById("left");
    left.appendChild(level);
}
function generateRandom(max) {
    return Math.floor(Math.random() * max) + 50;
}
function success() {
    count++;
    // clear old images 
    var left = document.getElementById("left");
    left.innerHTML = '';
    var right = document.getElementById("right");
    right.innerHTML = '';
    addLevel();
    addSameImages();
    addDifferentImage();
}
function fail() {
    // clear old images
    var left = document.getElementById("left");
    left.innerHTML = '';
    var right = document.getElementById("right");
    right.innerHTML = '';
    init();
}
