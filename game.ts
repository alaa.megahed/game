var count : number;
var img_src : string;

window.addEventListener("load", init);

function init() : void{
    count = 1;
    img_src = "image.jpeg";
    addLevel();
    addSameImages();
    addDifferentImage();
}

function addSameImages() : void{
    for(var i = 0; i < count; i++){
        var margin_left : number = generateRandom(screen.width / 2 - 200); 
        var margin_top  : number = generateRandom(screen.height -300); 
        for(var j = 0; j < 2; j++){    
            var img : HTMLImageElement = document.createElement("img");
            img.setAttribute("src", img_src);
            img.style.marginTop = margin_top + "px";
            img.style.marginLeft = margin_left + "px";
            img.addEventListener("click", fail);
            if(j == 0){
                var right : HTMLElement = document.getElementById("right");
                right.appendChild(img);
            } else {
                var left : HTMLElement = document.getElementById("left");
                left.appendChild(img);
            }          
        }
    }
}

function addDifferentImage() : void{
    var margin_left : number = generateRandom(screen.width / 2 - 200); 
    var margin_top : number = generateRandom(screen.height - 300); 
    var img : HTMLImageElement = document.createElement("img");
    img.setAttribute("src", img_src);
    img.style.marginTop = margin_top + "px";
    img.style.marginLeft = margin_left + "px";
    img.addEventListener("click", success);
    var left : HTMLElement = document.getElementById("left");
    left.appendChild(img);
}

function addLevel(){
    var level : HTMLElement = document.createElement("h3");
    level.innerHTML = "level: " + count;
    level.style.marginTop = "30px";
    level.style.marginLeft = "30px";
    level.style.color = "red";
    document.body.appendChild(level);
    var left : HTMLElement = document.getElementById("left");
    left.appendChild(level);
}


function generateRandom(max) : number{
    return Math.floor(Math.random() * max) + 50;
}

function success() : void{
    count++;
    // clear old images 
    var left : HTMLElement = document.getElementById("left");
    left.innerHTML = '';
    var right : HTMLElement= document.getElementById("right");
    right.innerHTML = '';
    addLevel();
    addSameImages();
    addDifferentImage();
}

function fail() : void{
    // clear old images
    var left : HTMLElement = document.getElementById("left");
    left.innerHTML = '';
    var right : HTMLElement = document.getElementById("right");
    right.innerHTML = '';
    init();
}